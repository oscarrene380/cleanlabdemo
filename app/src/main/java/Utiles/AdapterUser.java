package Utiles;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import example.com.cleanlabdemo.R;

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.ViewHolder> {

    public interface InterfaceUsers{
        void onListUserClickeado(String iduser);
    }
    private List<ItemUser> itemUsertList;
    private Context context;
    private InterfaceUsers _listener;

    public AdapterUser(List<ItemUser> itemUsertList, Context context, InterfaceUsers listener) {
        this.itemUsertList = itemUsertList;
        this.context = context;
        this._listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemUser itemUser = itemUsertList.get(position);
        final String idUseritem = itemUser.getIduser();

        holder.edtIdUserH.setText(itemUser.getIduser());
        holder.edtNombreH.setText("Nombre :"+itemUser.getNombre());
        holder.edtEdadH.setText("Edad :"+toString().valueOf(itemUser.getEdad()));
        holder.edtLabH.setText("Laboratorio : "+itemUser.getLaboratorio());
        holder.edtTipoH.setText("Tipo : "+itemUser.getTipoUsuario());
        holder.edtTurnoH.setText("Turno : "+itemUser.getTurno());
        holder.edtIdentificador.setText(itemUser.getIduser());

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_listener != null){
                    _listener.onListUserClickeado(idUseritem);
                }
            }

        });

    }

    @Override
    public int getItemCount() {
        return itemUsertList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

    public ItemUser _item;
    public TextView  edtIdUserH;
    public TextView edtNombreH;
    public TextView edtEdadH;
    public TextView edtTipoH;
    public TextView edtTurnoH;
    public TextView edtLabH;
    public TextView edtIdentificador;
    public Button btnEdit;

    public ViewHolder(View itemView) {
        super(itemView);
        edtIdUserH= (TextView) itemView.findViewById(R.id.iduser);
        edtNombreH= (TextView) itemView.findViewById(R.id.cardNombre);
        edtEdadH= (TextView) itemView.findViewById(R.id.cardEdad);
        edtTipoH= (TextView) itemView.findViewById(R.id.cardTipoUser);
        edtTurnoH= (TextView) itemView.findViewById(R.id.cardTurno);
        edtLabH= (TextView) itemView.findViewById(R.id.cardLaboratorio);
        edtIdentificador=(TextView)  itemView.findViewById(R.id.identificadoruserCard);
        btnEdit=(Button)  itemView.findViewById(R.id.btnCardEdituser);

    }

    public void bindearDatos(ItemUser item){
        _item = item;
        edtIdUserH.setText(item.getIduser());
    }

}
}
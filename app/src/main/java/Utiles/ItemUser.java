package Utiles;

import android.graphics.Bitmap;

public class ItemUser {

    private String iduser;
    private String Nombre;
    private int edad;
    private String Turno;
    private String TipoUsuario;
    private String Laboratorio;

    public ItemUser(String iduser, String nombre, int edad, String turno, String tipoUsuario, String laboratorio) {
        this.iduser = iduser;
        Nombre = nombre;
        this.edad = edad;
        Turno = turno;
        TipoUsuario = tipoUsuario;
        Laboratorio = laboratorio;
    }

    public String getIduser() {
        return iduser;
    }

    public String getNombre() {
        return Nombre;
    }

    public int getEdad() {
        return edad;
    }

    public String getTurno() {
        return Turno;
    }

    public String getTipoUsuario() {
        return TipoUsuario;
    }

    public String getLaboratorio() {
        return Laboratorio;
    }




}

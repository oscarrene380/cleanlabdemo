package Utiles;

public class Laboratorios {
    private int id;
    private String nombre;

    public Laboratorios(int id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return nombre;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Laboratorios){
            Laboratorios e = (Laboratorios ) obj;
            if(e.getNombre().equals(nombre) && e.getId()== id ) return true;
        }
        return false;
    }
}

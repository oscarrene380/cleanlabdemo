package Utiles;

//clase que maneja los endpoints
public class EndPoints {

    public static String EndPointUsuario ="https://servicetime.azurewebsites.net/Controllers/UsuarioController.php";
    public static String EndPointLaboratorios ="https://servicetime.azurewebsites.net/Controllers/LaboratorioController.php";
    public static String EndPointCuenta = "https://servicetime.azurewebsites.net/Controllers/CuentaController.php";
    public static String EndPointEdificio = "https://servicetime.azurewebsites.net/Controllers/EdificioController.php";
}

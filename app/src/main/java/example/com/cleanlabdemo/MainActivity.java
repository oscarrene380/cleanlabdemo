package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import Utiles.EndPoints;
import Utiles.VerificarConexion;

public class MainActivity extends AppCompatActivity {

    private EditText edtUsuario, edtClave;
    private ProgressDialog progreso;
    Button btnEntrar, btnSalir;
    String correo, clave;

    //NECESARIO MANDAR A LLAMAR LOS SIGUIENTES
    RequestQueue requestQueue;
    JsonArrayRequest jsonArrayRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtUsuario = findViewById(R.id.edtEmail);
        edtClave = findViewById(R.id.edtPass);
        btnEntrar = findViewById(R.id.btn_Login);
        btnSalir = findViewById(R.id.btn_Exit);

        progreso = new ProgressDialog(getApplicationContext());
        progreso.setTitle("Login");
        progreso.setCancelable(false);

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        // inicio de sesion automatico
        automaticLogin();
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                correo = edtUsuario.getText().toString().trim();
                clave = edtClave.getText().toString().trim();
                if(VerificarConexion.isNetDisponible(getApplicationContext()))
                {
                    if(correo.isEmpty())
                    {
                        edtUsuario.setError("Debe Ingresar Correo!");
                        edtUsuario.requestFocus();
                    }
                    if(!validarEmail(correo))
                    {
                        edtUsuario.setError("*debe ingresar un correo valido");
                        edtUsuario.requestFocus();
                    }
                    else if(clave.isEmpty())
                    {
                        edtClave.setError("Debe Ingresar Clave");
                        edtClave.requestFocus();
                    }
                    else
                    {
                        AutenticarUser(EndPoints.EndPointCuenta+"?user="+correo+"&password="+clave);
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Verifica tu Acceso a Internet ",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void automaticLogin()
    {
        SharedPreferences misPreferencias = getSharedPreferences("login",Context.MODE_PRIVATE);
        if(misPreferencias.contains("idUser") && misPreferencias.contains("clave"))
        {
            correo = misPreferencias.getString("idUser","");
            clave = misPreferencias.getString("clave","");
            AutenticarUser(EndPoints.EndPointCuenta+"?user="+correo+"&password="+clave);
        }
    }

    public void AutenticarUser(String URL)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //ESTO SE VA A EJECUTAR SI HAY RESPUESTA POR PARTE DEL WEBSERVICE
                JSONObject jsonObject = null;
                if(response.length() == 0)
                {
                    Toast.makeText(getApplicationContext(), "usuario o contraseña no validos", Toast.LENGTH_SHORT).show();
                    edtUsuario.requestFocus();
                }
                for (int i = 0; i < response.length(); i++) {
                    try
                    {
                        jsonObject = response.getJSONObject(i);
                        String idUser = jsonObject.getString("idUser");
                        String nombre = jsonObject.getString("nombre");
                        String apellido = jsonObject.getString("apellido");
                        int edad = jsonObject.getInt("edad");
                        int tipo = jsonObject.getInt("idTipo");
                        int idLab = jsonObject.getInt("idLab");
                        int activo = jsonObject.getInt("Activo");
                        if(activo == 1)
                        {
                            // datos del usuario
                            SharedPreferences misPreferencias = getSharedPreferences("login",Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = misPreferencias.edit();
                            editor.putString("idUser",idUser);
                            editor.putString("clave",clave);
                            editor.putString("nombre",nombre);
                            editor.putString("apellido",apellido);
                            editor.putInt("edad",edad);
                            editor.putInt("tipo",tipo);
                            editor.putInt("idLab",idLab);

                            editor.commit();
                            Intent intent = null;
                            switch (tipo)
                            {
                                case 1:
                                    intent = new Intent(getApplicationContext(),AdminMenu.class);
                                    break;
                                case 2:
                                    intent = new Intent(getApplicationContext(),EncarMenu.class);
                                    break;
                                case 3:
                                    intent = new Intent(getApplicationContext(),OrdenMenu.class);
                                    break;
                            }
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Esta cuenta no se encuentra activa",Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() { //ESTO SE VA A EJECUTAR SI NO HAY RESPUESTA POR PARTE DEL WEBSERVICE
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
        progressDialog.dismiss();
    }

}

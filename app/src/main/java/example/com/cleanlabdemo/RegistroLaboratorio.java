package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Utiles.Edificios;
import Utiles.EndPoints;
import Utiles.VerificarConexion;

public class RegistroLaboratorio extends AppCompatActivity
{
    private ArrayList<Edificios> edificios = new ArrayList<>();

    //NECESARIO MANDAR A LLAMAR LOS SIGUIENTES
    RequestQueue requestQueue;
    JsonArrayRequest jsonArrayRequest;
    EditText edtNombre;
    EditText edtDes;
    Spinner spEdi;
    Button btnGuardar;
    Button btnAtras;

    String nombreLab,descripcion;
    int idEdificio = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_laboratorio);
        edtNombre = findViewById(R.id.edtNombre);
        edtDes = findViewById(R.id.edtDes);
        spEdi = findViewById(R.id.spEdi);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnAtras = findViewById(R.id.btnAtras);
        ListaEdificios(EndPoints.EndPointEdificio);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombreLab = edtNombre.getText().toString().trim();
                descripcion = edtDes.getText().toString().trim();
                if(VerificarConexion.isNetDisponible(getApplicationContext()))
                {
                    if(TextUtils.isEmpty(nombreLab))
                    {
                        edtNombre.setError("*valor requerido");
                        edtNombre.requestFocus();
                    }
                    else if(TextUtils.isEmpty(descripcion))
                    {
                        edtDes.setError("*valor requerido");
                        edtDes.requestFocus();
                    }
                    else if(idEdificio == 0)
                    {
                        Toast.makeText(getApplicationContext(),"El edificio no es valido",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        insertarDatos(EndPoints.EndPointLaboratorios);
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Verifica tu Acceso a Internet ",Toast.LENGTH_LONG).show();
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent atras = new Intent(getApplicationContext(), AdminMenu.class);
                startActivity(atras);
                finish();
            }
        });

        spEdi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Edificios edificio = (Edificios) parent.getSelectedItem();
                //Toast.makeText(getApplicationContext(), "ID Edificio: "+edificio.getId()+" Nombre: "+edificio.getNombre(), Toast.LENGTH_SHORT).show();
                idEdificio = edificio.getId();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void ListaEdificios(String URL)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //ESTO SE VA A EJECUTAR SI HAY RESPUESTA POR PARTE DEL WEBSERVICE
                JSONObject jsonObject = null;
                if(response.length() == 0)
                {
                    edificios.add(new Edificios(0,"No hay edificios registrados") );
                    spEdi.setEnabled(false);
                }
                for (int i = 0; i < response.length(); i++) {
                    try
                    {
                        jsonObject = response.getJSONObject(i);
                        int id = jsonObject.getInt("idEdificio");
                        String nombre = jsonObject.getString("nombre");
                        edificios.add(new Edificios(id,nombre));
                    }
                    catch (JSONException e)
                    {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                //fill data in spinner
                ArrayAdapter<Edificios> adapter = new ArrayAdapter<Edificios>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, edificios);
                spEdi.setAdapter(adapter);
            }
        }, new Response.ErrorListener() { //ESTO SE VA A EJECUTAR SI NO HAY RESPUESTA POR PARTE DEL WEBSERVICE
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
        progressDialog.dismiss();
    }

    public void insertarDatos(String URL)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject obj = null;
                try
                {
                    obj = new JSONObject(response);
                    Toast.makeText(getApplicationContext(),obj.getString("msgs"),Toast.LENGTH_SHORT).show(); //MENSAJE DE CONFIRMACIÓN
                }
                catch (JSONException e)
                {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                edtNombre.setText(null);
                edtDes.setText(null);
                edtNombre.requestFocus();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //SI PETA PASA ESTO
                Toast.makeText(getApplicationContext(),"No se pudo registrar",Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {  //Acá creás un mapeo para poder establecer los parámetros del Método POST
                Map<String,String> parametros = new HashMap<String,String>();
                parametros.put("nombre",nombreLab);
                parametros.put("descripcion",descripcion);
                parametros.put("idEdificio",String.valueOf(idEdificio));
                return parametros;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        progressDialog.dismiss();
    }
}

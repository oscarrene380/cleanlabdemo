package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class OrdenMenu extends AppCompatActivity {

    Button ordAseo, labAsignados, histOrdenes, cerrar, btnProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orden_menu);

        ordAseo = findViewById(R.id.btnPeticion);
        labAsignados = findViewById(R.id.btnLabAsig);
        histOrdenes = findViewById(R.id.btnLab);
        cerrar = findViewById(R.id.btn_Exit);
        btnProfile = findViewById(R.id.btnProfile);

        ordAseo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent objOrdAseo = new Intent(getApplicationContext(),OrdenAseo.class);
                startActivity(objOrdAseo);
                finish();
            }
        });

        labAsignados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent objlabAsignados = new Intent(getApplicationContext(),LaboratoriosAsignados.class);
                startActivity(objlabAsignados);
                finish();
            }
        });

        histOrdenes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regUsuario = new Intent(getApplicationContext(),HistorialView.class);
                startActivity(regUsuario);
                finish();
            }
        });

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent perfil = new Intent(getApplicationContext(),PerfilUser.class);
                startActivity(perfil);
                finish();
            }
        });

        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences misPreferencias = getSharedPreferences("login", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = misPreferencias.edit();
                editor.remove("idUser");
                editor.remove("clave");
                editor.remove("nombre");
                editor.remove("apellido");
                editor.remove("edad");
                editor.remove("tipo");
                editor.remove("idLab");
                editor.commit();
                Intent objCerrar = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(objCerrar);
                finish();
            }
        });

    }

}

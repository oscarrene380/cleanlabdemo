package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EncarMenu extends AppCompatActivity {

    Button progLimpieza, horarios, cerrar, btnProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encar_menu);

        progLimpieza = findViewById(R.id.btnPro);
        horarios = findViewById(R.id.btnHorario);
        cerrar = findViewById(R.id.btn_Exit);
        btnProfile = findViewById(R.id.btnProfile);

        progLimpieza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent programarLimp = new Intent(getApplicationContext(),PeticionLimpieza.class);
                startActivity(programarLimp);
                finish();
            }
        });

        horarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent verhorarios = new Intent(getApplicationContext(),VerHorariosLimpieza.class);
                startActivity(verhorarios);
                finish();
            }
        });

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent perfil = new Intent(getApplicationContext(),PerfilUser.class);
                startActivity(perfil);
                finish();
            }
        });

        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences misPreferencias = getSharedPreferences("login", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = misPreferencias.edit();
                editor.remove("idUser");
                editor.remove("clave");
                editor.remove("nombre");
                editor.remove("apellido");
                editor.remove("edad");
                editor.remove("tipo");
                editor.remove("idLab");
                editor.commit();
                Intent objCerrar = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(objCerrar);
                finish();
            }
        });

    }
}

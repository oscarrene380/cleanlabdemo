package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.JarURLConnection;
import java.net.URL;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import Utiles.EndPoints;
import Utiles.Laboratorios;
import Utiles.VerificarConexion;

public class RegistroUsuario extends AppCompatActivity {

    Button btnBack,btnSafe ;
    EditText edtCorreo,edtNombre,edtApellido,edtEdad;
    Spinner spGenero,spTurno,spRol,spLaboratorio;
    int[] items_value_roles = new int[]{ 1, 2, 3};
    int[] items_value_turno = new int[]{ 1, 2, 3};
    String[] items_value_genero = new String[]{"M","F"};
    int idLabSelect=0;

    private ArrayList<Laboratorios> lstLaboratorios = new ArrayList<>();

    RequestQueue requestQueue;
    JsonArrayRequest jsonArrayRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        if(!VerificarConexion.isNetDisponible(getApplicationContext())){
            Toast.makeText(getApplicationContext(),"Algunas funcionalidades no se cargaron correctamente ya que no posee acceso a internet",Toast.LENGTH_SHORT).show();
        }

        spGenero = findViewById(R.id.spGenero);
        spRol = findViewById(R.id.spRol);
        spTurno = findViewById(R.id.spTurno);
        spLaboratorio = findViewById(R.id.spLaboratorio);

        CargarGenero();
        CargarRoles();
        CargarTurno();
        CargarLaboratorios();

        btnBack = findViewById(R.id.btnBack);
        btnSafe = findViewById(R.id.btnSafe);
        edtCorreo = findViewById(R.id.edtCorreo);
        edtNombre = findViewById(R.id.edtNombre);
        edtApellido = findViewById(R.id.edtApellido);
        edtEdad = findViewById(R.id.edtEdad);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent atras = new Intent(getApplicationContext(), AdminMenu.class);
                startActivity(atras);
                finish();
            }
        });

        btnSafe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String genero = items_value_genero[spGenero.getSelectedItemPosition()];
                int rol = items_value_roles[spRol.getSelectedItemPosition()];
                int turno=items_value_turno[spTurno.getSelectedItemPosition()];
                String user = edtCorreo.getText().toString();
                String nombre = edtNombre.getText().toString();
                String apellido = edtApellido.getText().toString();

                if(VerificarConexion.isNetDisponible(getApplicationContext())){
                    if(TextUtils.isEmpty(user))
                    {
                        edtCorreo.setError("*valor requerido");
                        edtCorreo.requestFocus();
                    }
                    if(!validarEmail(user))
                    {
                        edtCorreo.setError("*debe ingresar un correo valido");
                        edtCorreo.requestFocus();
                    }
                    else if(TextUtils.isEmpty(nombre))
                    {
                        edtNombre.setError("*valor requerido");
                        edtNombre.requestFocus();
                    }
                    else if(TextUtils.isEmpty(apellido))
                    {
                        edtApellido.setError("*valor requerido");
                        edtApellido.requestFocus();
                    }
                    else if(TextUtils.isEmpty(edtEdad.getText().toString()))
                    {
                        edtEdad.setError("*valor requerido");
                        edtEdad.requestFocus();
                    }
                    else
                    {
                        int edad= Integer.parseInt(edtEdad.getText().toString());
                        Crearusuario(user,nombre,apellido,edad,genero,turno,rol,idLabSelect);
                    }
                }
            }
        });

        spLaboratorio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Laboratorios laboratorios = (Laboratorios) parent.getSelectedItem();
                idLabSelect = laboratorios.getId();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

   public void ValidarDatos(){

   }
    //carga los roles
    public void CargarRoles(){

        //Lleno el spinner con el tipo de usuario
        String[] items = new String[]{ "administrador", "Encargado", "Ordenanza"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spRol.setAdapter(adapter);
    }

    //carga el genero
    public void CargarGenero(){
        //Lleno el spinner de spGenero
        String[] items = new String[]{ "Masculino", "Femenino"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spGenero.setAdapter(adapter2);
    }

    //carga los turnos
    public void CargarTurno(){
        //Lleno el spinner de spGenero
        String[] items = new String[]{ "Mañana", "Tarde","Noche"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spTurno.setAdapter(adapter2);
    }

    //metodo carga los laboratorios desde le endpoint
    public void CargarLaboratorios()
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(EndPoints.EndPointLaboratorios, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //ESTO SE VA A EJECUTAR SI HAY RESPUESTA POR PARTE DEL WEBSERVICE
                JSONObject jsonObject = null;
                if(response.length() == 0)
                {
                     lstLaboratorios.add(new Laboratorios(0,"No hay edificios registrados") );
                    spLaboratorio.setEnabled(false);
                }
                for (int i = 0; i < response.length(); i++) {
                    try
                    {
                        jsonObject = response.getJSONObject(i);
                        int id = jsonObject.getInt("idLab");
                        String nombre = jsonObject.getString("laboratorio");
                        lstLaboratorios.add(new Laboratorios(id,nombre));
                    }
                    catch (JSONException e)
                    {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                //fill data in spinner
                ArrayAdapter<Laboratorios> adapter = new ArrayAdapter<Laboratorios>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,lstLaboratorios);
                spLaboratorio.setAdapter(adapter);
            }
        }, new Response.ErrorListener() { //ESTO SE VA A EJECUTAR SI NO HAY RESPUESTA POR PARTE DEL WEBSERVICE
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue = Volley.newRequestQueue(this); //NOTA: ANDÁ AL buil.gradle:module y agregá: implementation 'com.android.volley:volley:1.1.0' PARA PODER OCUPAR LA LIBRERÍA DE CONEXIÓN A SERVICIOS EXTERNOS
        requestQueue.add(jsonArrayRequest);
        progressDialog.dismiss();

    }

    //Metodo crea un usuario
    public void Crearusuario(final String idUser,final String nombre, final String apellido, final int edad ,
                             final String genero,final int idTurno, final int idTipo, final int idLab){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.EndPointUsuario, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject obj = null;
                try
                {
                    obj = new JSONObject(response);
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),obj.getString("msgs"),Toast.LENGTH_SHORT).show(); //MENSAJE DE CONFIRMACIÓN
                    edtCorreo.setText(null);
                    edtNombre.setText(null);
                    edtApellido.setText(null);
                    edtEdad.setText(null);
                    edtCorreo.requestFocus();
                    spGenero.setSelection(0);
                    spRol.setSelection(0);
                    spTurno.setSelection(0);
                }
                catch (JSONException e)
                {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                //SI PETA PASA ESTO
                Toast.makeText(getApplicationContext(),"No se pudo registrar",Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Acá creás un mapeo para poder establecer los parámetros del Método POST
                Map<String,String> parametros = new HashMap<String,String>();
                parametros.put("idUser",idUser);
                parametros.put("nombre",nombre);
                parametros.put("apellido",apellido);
                parametros.put("edad",String.valueOf(edad));
                parametros.put("genero",genero);
                parametros.put("idTurno",String.valueOf(idTurno));
                parametros.put("idTipo",String.valueOf(idTipo));
                parametros.put("idLab",String.valueOf(idLab));
                return parametros;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}

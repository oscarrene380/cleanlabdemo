package example.com.cleanlabdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Utiles.AdapterUser;
import Utiles.EndPoints;
import Utiles.ItemUser;

public class GestionUsuarios extends AppCompatActivity implements AdapterUser.InterfaceUsers {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ItemUser> itemUserList;
    RequestQueue requestQueue;
    JsonArrayRequest jsonArrayRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_usuarios);

        recyclerView = (RecyclerView) findViewById(R.id.reciclerlista);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        CargarDatos();

        CargarDatos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_usuario,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.btnNuevo:
                Intent nuevo = new Intent(getApplicationContext(),RegistroUsuario.class);
                startActivity(nuevo);
                finish();
                break;

            case  R.id.btnAtras:
                Intent atras = new Intent(getApplicationContext(),AdminMenu.class);
                startActivity(atras);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void configurarRecycler(List<ItemUser> itemUserList) {
        adapter = new AdapterUser(itemUserList,getApplicationContext(), GestionUsuarios.this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onListUserClickeado(String iduser) {

    }

    private void CargarDatos(){
        itemUserList = new ArrayList<>();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(EndPoints.EndPointUsuario,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //ESTO SE VA A EJECUTAR SI HAY RESPUESTA POR PARTE DEL WEBSERVICE
                JSONObject jsonObject = null;
                if(response.length() == 0)
                {
                    Toast.makeText(getApplicationContext(), "No hay edificios registrados", Toast.LENGTH_SHORT).show();
                }

                for (int i = 0; i < response.length(); i++) {
                    try
                    {
                        jsonObject = response.getJSONObject(i);
                        String iduser = jsonObject.getString("idUser");
                        String nombre = jsonObject.getString("nombre");
                        String apellido = jsonObject.getString("apellido");
                        String turno = jsonObject.getString("turno");
                        String laboratorio = jsonObject.getString("laboratorio");
                        String tipouser = jsonObject.getString("tipo");
                        int edad =Integer.parseInt(jsonObject.getString("edad"));

                        String nombreCompleto = nombre + " " + apellido;
                        ItemUser itemReport = new ItemUser(iduser,nombreCompleto,edad,turno,tipouser,laboratorio);

                        itemUserList.add(itemReport);

                }
                    catch (JSONException e)
                    {
                        Log.i("error",e.toString());
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                configurarRecycler(itemUserList);
            }
        }, new Response.ErrorListener() { //ESTO SE VA A EJECUTAR SI NO HAY RESPUESTA POR PARTE DEL WEBSERVICE
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error",error.toString());
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
        progressDialog.dismiss();

    }
}

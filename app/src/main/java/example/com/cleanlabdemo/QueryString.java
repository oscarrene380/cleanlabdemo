package example.com.cleanlabdemo;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class QueryString
{
    /*Esta clase nos ayudara a concatenar las variables para enviarlas por url (GET)*/
    /*
        EJEMPLO DE USO
        public static void main(String[] args)
        {
            QueryString qs = new QueryString("pg", "q");
            qs.add("kl", "XX");
            qs.add("stype", "stext");
            qs.add("q", "+\"Java Programming\"");
            String url = "http://www.java.com/query?" + qs;
            System.out.println(url); //esta es la url ya armada
        }
    */
    private String query = "";

    public QueryString(String name, String value)
    {
        encode(name, value);
    }

    public void add(String name, String value)
    {
        query += "&";
        encode(name, value);
    }

    private void encode(String name, String value)
    {
        try
        {
            query +=URLEncoder.encode(name, "UTF-8");
            query += "=";
            query += URLEncoder.encode(value, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            throw new RuntimeException("Broken VM does not support UTF-8");
        }
    }

    public String getQuery()
    {
        return query;
    }

    public String toString()
    {
        return getQuery();
    }
}

package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AdminMenu extends AppCompatActivity {

    Button usuarios, laboratorios, edificios, btnPerfil, cerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_menu);

        usuarios = findViewById(R.id.btnUsuarios);
        laboratorios = findViewById(R.id.btnLab);
        edificios = findViewById(R.id.btnEdificios);
        btnPerfil = findViewById(R.id.btnProfile);
        cerrar = findViewById(R.id.btn_Exit);

        usuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regUsuario = new Intent(getApplicationContext(),GestionUsuarios.class);
                startActivity(regUsuario);
                finish();
            }
        });

        edificios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regEdificio = new Intent(getApplicationContext(),GestionEdificios.class);
                startActivity(regEdificio);
                finish();
            }
        });

        laboratorios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regLaboratorio = new Intent(getApplicationContext(),GestionLaboratorios.class);
                startActivity(regLaboratorio);
                finish();
            }
        });

        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent perfil = new Intent(getApplicationContext(),PerfilUser.class);
                startActivity(perfil);
                finish();
            }
        });

        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences misPreferencias = getSharedPreferences("login", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = misPreferencias.edit();
                editor.remove("idUser");
                editor.remove("clave");
                editor.remove("nombre");
                editor.remove("apellido");
                editor.remove("edad");
                editor.remove("tipo");
                editor.remove("idLab");
                editor.commit();
                Intent objCerrar = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(objCerrar);
                finish();
            }
        });
    }
}

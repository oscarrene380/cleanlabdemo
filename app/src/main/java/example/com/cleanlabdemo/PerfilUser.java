package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Utiles.EndPoints;
import Utiles.VerificarConexion;

public class PerfilUser extends AppCompatActivity
{
    EditText edtUsuario, edtNombre, edtLaboratorio, edtNuevaClave, edtConfirmar;
    Button btnGuardar, btnSalir;

    String clave, confirmar, respuesta;
    int tipo;

    //NECESARIO MANDAR A LLAMAR LOS SIGUIENTES
    RequestQueue requestQueue;
    JsonArrayRequest jsonArrayRequest;

    private ProgressDialog progreso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_user);
        edtUsuario = findViewById(R.id.edtUsuario);
        edtNombre = findViewById(R.id.edtNombre);
        edtLaboratorio = findViewById(R.id.edtLaboratorio);
        edtNuevaClave = findViewById(R.id.edtNuevaClave);
        edtConfirmar = findViewById(R.id.edtConfirmar);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnSalir = findViewById(R.id.btnSalir);

        final SharedPreferences misPrefrencias = getSharedPreferences("login", Context.MODE_PRIVATE);
        edtUsuario.setText(misPrefrencias.getString("idUser",""));
        edtNombre.setText(misPrefrencias.getString("nombre","")+" "+misPrefrencias.getString("apellido",""));
        ConsultarLaboratorio(EndPoints.EndPointUsuario+"?"+"r=1&buscar="+misPrefrencias.getString("idUser",""));
        tipo = misPrefrencias.getInt("tipo",0);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar(edtNuevaClave) && validar(edtConfirmar))
                {
                    clave =  edtNuevaClave.getText().toString();
                    confirmar = edtConfirmar.getText().toString().trim();
                    if(!clave.equals(confirmar))
                    {
                        edtConfirmar.setError("*La clave no coincide");
                        edtConfirmar.requestFocus();
                    }
                    else
                    {
                        if(VerificarConexion.isNetDisponible(getApplicationContext()))
                        {
                            final String iduser = misPrefrencias.getString("idUser","");
                            ResetPassword(iduser,clave);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Verifica tu conexion a internet",Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (tipo)
                {
                    case 1:
                        intent = new Intent(getApplicationContext(),AdminMenu.class);
                        break;
                    case 2:
                        intent = new Intent(getApplicationContext(),EncarMenu.class);
                        break;
                    case 3:
                        intent = new Intent(getApplicationContext(),OrdenMenu.class);
                        break;
                }
                startActivity(intent);
                finish();
            }
        });
    }

    private boolean validar(EditText edt)
    {
        if(TextUtils.isEmpty(edt.getText().toString().trim()))
        {
            edt.setError("*valor requerido");
            edt.requestFocus();
            return false;
        }
        return true;
    }

    public void ConsultarLaboratorio(String URL)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response)
            { //ESTO SE VA A EJECUTAR SI HAY RESPUESTA POR PARTE DEL WEBSERVICE
                JSONObject jsonObject = null;
                try
                {
                    jsonObject = response.getJSONObject(0);
                    respuesta = jsonObject.getString("laboratorio");
                    edtLaboratorio.setText(respuesta);
                }
                catch (JSONException e)
                {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() { //ESTO SE VA A EJECUTAR SI NO HAY RESPUESTA POR PARTE DEL WEBSERVICE
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
                respuesta = error.toString();
                edtLaboratorio.setText(respuesta);
            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
        progressDialog.dismiss();
    }

    public void  ResetPassword(final String usuarioid, final String clave)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST,EndPoints.EndPointCuenta, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject obj = null;
                try
                {
                    obj = new JSONObject(response);
                    Toast.makeText(getApplicationContext(),obj.getString("msgs"),Toast.LENGTH_SHORT).show(); //MENSAJE DE CONFIRMACIÓN
                    if(obj.getString("msgs").equals("La contraseña se cambio con exito"))
                    {
                        SharedPreferences preferencias = getSharedPreferences("login",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferencias.edit();
                        editor.putString("clave",clave);
                        editor.commit();
                    }
                }
                catch (JSONException e)
                {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                edtNuevaClave.setText(null);
                edtConfirmar.setText(null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                //SI PETA PASA ESTO
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Acá creás un mapeo para poder establecer los parámetros del Método POST
                Map<String,String> parametros = new HashMap<String,String>();
                parametros.put("user",usuarioid);
                parametros.put("password",clave);
                return parametros;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        progressDialog.dismiss();
    }
}

package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class PeticionLimpieza extends AppCompatActivity {

    Button btnSalir;
    EditText inicio, fin;
    TimePickerDialog tpdTurno;
    Calendar c;
    int hora, minuto;
    String amPm;
    Context nContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peticion_limpieza);

        btnSalir = findViewById(R.id.btnBack);
        inicio = findViewById(R.id.edtInicio);
        fin = findViewById(R.id.edtFin);
        Calendar calendar = Calendar.getInstance();

        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final  int minute = calendar.get(Calendar.MINUTE);

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent atras = new Intent(getApplicationContext(), EncarMenu.class);
                startActivity(atras);
                finish();
            }
        });

        inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TimePickerDialog timePickerDialog = new TimePickerDialog(nContext, new TimePickerDialog.OnTimeSetListener(){
                    @Override
                    public  void onTimeSet(TimePicker view, int hourOfDay, int minute){
                        inicio.setText(hourOfDay + ":"+minute);
                    }
                },hour,minute, DateFormat.is24HourFormat(nContext));
                timePickerDialog.show();
            }
        });

        fin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(nContext, new TimePickerDialog.OnTimeSetListener(){
                    @Override
                    public  void onTimeSet(TimePicker view, int hourOfDay, int minute){
                        fin.setText(hourOfDay + ":"+minute);
                    }
                },hour,minute, DateFormat.is24HourFormat(nContext));
                timePickerDialog.show();
            }
        });

    }

}

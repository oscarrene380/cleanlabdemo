package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Utiles.EndPoints;

public class GestionLaboratorios extends AppCompatActivity {

    private ListView lvLaboratorios;
    private ArrayList<String> laboratorios = new ArrayList<String>();

    //NECESARIO MANDAR A LLAMAR LOS SIGUIENTES
    RequestQueue requestQueue;
    JsonArrayRequest jsonArrayRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_laboratorios);
        lvLaboratorios = findViewById(R.id.lvLaboratorio);
        ListaLaboratorios(EndPoints.EndPointLaboratorios);

        lvLaboratorios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(),item,Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void ListaLaboratorios(String URL)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //ESTO SE VA A EJECUTAR SI HAY RESPUESTA POR PARTE DEL WEBSERVICE
                JSONObject jsonObject = null;
                if(response.length() == 0)
                {
                    Toast.makeText(getApplicationContext(), "No hay laboratorios registrados", Toast.LENGTH_SHORT).show();
                }

                for (int i = 0; i < response.length(); i++) {
                    try
                    {
                        jsonObject = response.getJSONObject(i);
                        String nombre = jsonObject.getString("laboratorio");
                        String edificio = jsonObject.getString("edificio");
                        laboratorios.add("Laboratorio: "+nombre+"\nEdificio: "+edificio);
                    }
                    catch (JSONException e)
                    {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, laboratorios);
                lvLaboratorios.setAdapter(adapter);
            }
        }, new Response.ErrorListener() { //ESTO SE VA A EJECUTAR SI NO HAY RESPUESTA POR PARTE DEL WEBSERVICE
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
        progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_usuario,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.btnNuevo:
                Intent nuevo = new Intent(getApplicationContext(),RegistroLaboratorio.class);
                startActivity(nuevo);
                finish();
                break;


            case  R.id.btnAtras:
                Intent atras = new Intent(getApplicationContext(),AdminMenu.class);
                startActivity(atras);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

package example.com.cleanlabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Utiles.EndPoints;
import Utiles.VerificarConexion;

public class RegistroEdificio extends AppCompatActivity {
    EditText edtNombre;
    EditText edtUbicacion;
    Button btnGuardar;
    Button btnAtras;

    String nombreE,ubicacion;

    //NECESARIO MANDAR A LLAMAR LOS SIGUIENTES
    RequestQueue requestQueue;
    JsonArrayRequest jsonArrayRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_edificio);
        edtNombre = findViewById(R.id.edtNombre);
        edtUbicacion = findViewById(R.id.edtUbicacion);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnAtras = findViewById(R.id.btnBack);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombreE = edtNombre.getText().toString().trim();
                ubicacion = edtUbicacion.getText().toString().trim();
                if(VerificarConexion.isNetDisponible(getApplicationContext()))
                {
                    if(TextUtils.isEmpty(nombreE))
                    {
                        edtNombre.setError("*campo requerido");
                        edtNombre.requestFocus();
                    }
                    else if(TextUtils.isEmpty(ubicacion))
                    {
                        edtUbicacion.setError("*campo requerido");
                        edtUbicacion.requestFocus();
                    }
                    else
                    {
                        insertarDatos(EndPoints.EndPointEdificio);
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Verifica tu Acceso a Internet ",Toast.LENGTH_LONG).show();
                }
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent atras = new Intent(getApplicationContext(), AdminMenu.class);
                startActivity(atras);
                finish();
            }
        });
    }

    public void insertarDatos(String URL)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject obj = null;
                try
                {
                    obj = new JSONObject(response);
                    Toast.makeText(getApplicationContext(),obj.getString("msgs"),Toast.LENGTH_SHORT).show(); //MENSAJE DE CONFIRMACIÓN
                }
                catch (JSONException e)
                {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                edtNombre.setText(null);
                edtUbicacion.setText(null);
                edtNombre.requestFocus();
            }
        }, new Response.ErrorListener() {
           @Override
            public void onErrorResponse(VolleyError error) { //SI PETA PASA ESTO
                Toast.makeText(getApplicationContext(),"No se pudo registrar",Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {  //Acá creás un mapeo para poder establecer los parámetros del Método POST
                Map<String,String> parametros = new HashMap<String,String>();
                parametros.put("nombre",nombreE);
                parametros.put("descripcion",ubicacion);
                return parametros;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        progressDialog.dismiss();
    }
}
